export const calculateEllipseField = (a: number, b: number): number => {
    return Math.round(Math.PI * (a / 2) * (b / 2))
}