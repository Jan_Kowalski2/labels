export const initialResponseStatus: ResponseStatus = {
    isLoading: false,
    isLoaded: false,
    isError: false
}

export const loadingResponseStatus: ResponseStatus = {
    isLoading: true,
    isLoaded: false,
    isError: false
}

export const successResponseStatus: ResponseStatus = {
    isLoading: false,
    isLoaded: true,
    isError: false
}

export const errorResponseStatus: ResponseStatus = {
    isLoading: false,
    isLoaded: false,
    isError: true
}