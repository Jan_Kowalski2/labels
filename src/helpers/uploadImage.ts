import { API } from "env"

interface ImageUploadResponse {
    access_mode: string
    bytes: number
    created_at: string
    etag: string
    format: string
    height: number
    original_filename: string
    placeholder: boolean
    public_id: string
    resource_type: string
    secure_url: string
    signature: string
    tags: string[]
    type: string
    url: string
    version: number
    width: number
}

export const uploadImage = (file: File, onChange: (state: UploadState) => void, onSuccess: (file: UploadedFile) => void): void => {
    const formData = new FormData()

    const preparedData = {
        "api_key": API.apiKey,
        "upload_preset": API.uploadPreset,
        file
    }
            
    Object.entries(preparedData).forEach(entry => {
        const [ key, value ] = entry
        formData.append(key, value)
    })

    const xhr = new XMLHttpRequest()

    xhr.responseType = "json"

    xhr.upload.onprogress = event => {
        const { lengthComputable, loaded, total } = event
        if (lengthComputable) {
            let progress = Math.round(100 * loaded / total);
            onChange({
                progress,
                error: "",
            })
        }
        else {
            onChange({
                progress: 0,
                error: ""
            })
        }
    }

    xhr.onreadystatechange = () => {
        if (xhr.readyState === 4 && xhr.status === 200) {
            const response: ImageUploadResponse = xhr.response
            const { 
                public_id: fileId, 
                original_filename,
                format,
                url, 
            } = response 

            onSuccess({
                fileId,
                fileName: `${original_filename}.${format}`,
                url
            })
        }
        else {
            onChange({
                progress: 100,
                error: xhr.statusText
            })
        }
    }

    xhr.upload.onerror = () => {
        onChange({ 
            progress: 100,
            error: xhr.responseText
        })
    }

    xhr.open("POST", API.baseUrl, true)
    xhr.send(formData)
}

export default uploadImage