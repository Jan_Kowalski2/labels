import { LOCAL_STORAGE_IMAGES_KEY } from "redux/db/types";

const getLocalImages = (): LocalImage[] => {
    const receivedStorageData: string|null = localStorage.getItem(LOCAL_STORAGE_IMAGES_KEY)

    if (typeof receivedStorageData === "string") {
        return JSON.parse(receivedStorageData)
    }

    return []
}

const setLocalImages = (newLocalImages: LocalImage[]): void => {
    localStorage.setItem(LOCAL_STORAGE_IMAGES_KEY, JSON.stringify(newLocalImages))
}

export default {
    get: getLocalImages,
    set: setLocalImages
}