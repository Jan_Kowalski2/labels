export const calculateImageFitToCanvas = (parent: Dimensions, child: Dimensions): Dimensions => {
    const isChildSideWider = child.width > child.height
    if (isChildSideWider) {
        return {
            width: parent.width,
            height: Math.round((child.height / child.width) * parent.width)
        }
    }
    
    return {
        height: parent.height,
        width: Math.round((child.width / child.height) * parent.height)
    }   
}

export default calculateImageFitToCanvas