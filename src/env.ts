if (!process.env.REACT_APP_API_URL) {
    throw new Error('Please set REACT_APP_API_URL variable in your env.')
}

if (!process.env.REACT_APP_API_KEY) {
    throw new Error('Please set REACT_APP_API_KEY variable in your env.')
}

if (!process.env.REACT_APP_API_UPLOAD_PRESET) {
    throw new Error('Please set REACT_APP_API_UPLOAD_PRESET variable in your env.')
}

export const API = {
    baseUrl: process.env.REACT_APP_API_URL,
    apiKey: process.env.REACT_APP_API_KEY,
    uploadPreset: process.env.REACT_APP_API_UPLOAD_PRESET
}
