import React from "react"
import { Link } from "react-router-dom"
import { getStyles } from "helpers";
import styles from "./notfound.module.css"

const NotFound = (): JSX.Element => {
    const cx = getStyles(styles)

    return (
        <div className={cx("root")}>
            <div className={cx("holder")}>
                <h2>404</h2>
                <p>
                    Trafiłeś do złej dzielnicy.
                    <br />
                    Kliknij <Link to="/">tutaj</Link>, aby powrócić do biblioteki zdjęć.
                </p>
            </div>
        </div>
    )
}

export default NotFound