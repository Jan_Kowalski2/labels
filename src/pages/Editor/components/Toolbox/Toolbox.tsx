import React from "react"
import { useSelector, useDispatch } from "react-redux"

import { AppState } from "redux/store"
import { editImage } from "redux/db/actions"
import { setChangeFlag } from "redux/editor/actions";

import { getStyles } from "helpers";
import { Tools, SelectedArea, Legend, Info } from "../index"
import styles from "./toolbox.module.css"


const Toolbox = (): JSX.Element => {
    const cx = getStyles(styles)
    const isChanged = useSelector<AppState, boolean>(state => state.editor.isChanged)
    const image = useSelector<AppState, LocalImage|null>((state: AppState) => state.editor.current)
    const dispatch = useDispatch()

    const handleSave = () => {
        dispatch(setChangeFlag(false))
        if (image) {
            dispatch(editImage(image))
        }
    }
    return (
        <aside className={cx("root")}>
            <header className={cx("header")}>
                <h2>Przybornik</h2>
                {isChanged && <button type="button" className={cx("save")} onClick={handleSave}>Zapisz zmiany</button>}
            </header>
            <ul>
                <li>
                    <strong>Narzędzia do zaznaczania</strong>
                    <Tools />
                </li>
                <li>
                    <strong>Zaznaczony obszar</strong>
                    <SelectedArea />
                </li>
                <li>
                    <strong>Legenda</strong>
                    <Legend />
                </li>
                <li>
                    <strong>Informacje o obrazie</strong>
                    <Info />
                </li>
            </ul>
        </aside>
    )
}

export default Toolbox