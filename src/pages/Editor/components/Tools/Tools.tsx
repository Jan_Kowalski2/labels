import React from "react"
import { useSelector, useDispatch } from "react-redux"
import { AppState } from "redux/store"
import { setFigureType } from "redux/editor/actions";
import { getStyles } from "helpers";
import styles from "./tools.module.css"


const Tools = (): JSX.Element => {
    
    const cx = getStyles(styles)
    const figureType = useSelector<AppState, FigureType>((state: AppState) => state.editor.figureType)
    const dispatch = useDispatch()

    return (
        <ul className={cx("root")}>
            <li 
                className={cx({ active: figureType === "RECTANGLE"})} 
                title="Narzędzie do zaznaczania: Prostokąt"
                onClick={() => dispatch(setFigureType("RECTANGLE"))}
            >
                <svg aria-hidden="true" viewBox="0 0 448 512">
                    <path fill="currentColor" d="M400 32H48C21.5 32 0 53.5 0 80v352c0 26.5 21.5 48 48 48h352c26.5 0 48-21.5 48-48V80c0-26.5-21.5-48-48-48zm-6 400H54c-3.3 0-6-2.7-6-6V86c0-3.3 2.7-6 6-6h340c3.3 0 6 2.7 6 6v340c0 3.3-2.7 6-6 6z"></path>
                </svg>
            </li>
            <li 
                className={cx({ active: figureType === "ELLIPSE"})}
                title="Narzędzie do zaznaczania: Elipsa"
                onClick={() => dispatch(setFigureType("ELLIPSE"))}
            >
                <svg aria-hidden="true" viewBox="0 0 512 512">
                    <path fill="currentColor" d="M256 8C119 8 8 119 8 256s111 248 248 248 248-111 248-248S393 8 256 8zm0 448c-110.5 0-200-89.5-200-200S145.5 56 256 56s200 89.5 200 200-89.5 200-200 200z"></path>
                </svg>
            </li>
        </ul>
    )
}

export default Tools