import React from "react"
import { useSelector } from "react-redux"

import { AppState } from "redux/store"
import { getStyles } from "helpers";
import styles from "./legend.module.css"
    
const Legend = (): JSX.Element => {
    const cx = getStyles(styles)

    const image = useSelector<AppState, LocalImage|null>((state: AppState) => state.editor.current)

    if (!image || image.categories.length === 0) {
        return (
            <p>Obecnie brak przypisanych kategorii.</p>
        )
    }

    return (
        <ul className={cx("root")}>
            {image.categories.map(category => {
                const { title, color } = category
                return (
                    <li key={title}>
                        <span className={cx("color")} style={{ backgroundColor: color }}></span>
                        <p>{title}</p>
                    </li>
                )
            })}
        </ul>
    )
}
export default Legend