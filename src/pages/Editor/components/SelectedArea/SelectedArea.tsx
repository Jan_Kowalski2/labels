import React, { useEffect, useState } from "react"
import { useSelector, useDispatch } from "react-redux"

import { AppState } from "redux/store"
import { Input } from "ui"

import { getStyles } from "helpers"
import styles from "./selectedarea.module.css"
import { FIGURE_NAMES } from "constants/index"

import { HTMLCanvas } from "../Canvas/HTMLCanvas"
import { setCategory } from "redux/editor/actions"

const SelectedArea = (): JSX.Element|null => {
    const cx = getStyles(styles)

    const [ categoryValue, setCategoryValue ] = useState("")

    const image = useSelector<AppState, LocalImage|null>(state => state.editor.current)
    const currentLabelIndex = useSelector<AppState, number | null>((state: AppState) => state.editor.activeFigureIndex)
    const dispatch = useDispatch()

    useEffect(() => {
        if (currentLabelIndex !== null && image) {
            setCategoryValue(image.labels[currentLabelIndex].category.title)
        }
    }, [ currentLabelIndex, image ])

    if (!image) {
        return null
    }


    if (currentLabelIndex === null) {
        return (
            <p>
                Kliknij na stworzoną figurę, aby wyświetlić informacje o niej.
                Kolor kategorii zostanie przydzielony automatycznie.
            </p>
        )
    }

    const { figure } = image.labels[currentLabelIndex]
    const { type, start, end } = figure

    const figurePosition: Rectangle = HTMLCanvas.getFigurePosition(start, end)
    const fieldDimensions: number = HTMLCanvas.getFigureField(type, start, end)

    return (
        <section className={cx("root")}>
            <p>Kolor kategorii zostanie przydzielony automatycznie.</p>
            <div className={cx("formHolder")}>
                <Input 
                    id="addCategory" 
                    label="Nazwa kategorii"
                    value={categoryValue}
                    onChange={(event => setCategoryValue(event.target.value))}
                />
                <button 
                    type="button" 
                    className={cx("addCategory")}
                    // @ts-ignore
                    onClick={() => dispatch(setCategory(currentLabelIndex, categoryValue))}
                >
                    Zapisz
                </button>
            </div>
            <dl>
                <dt>Typ figury:</dt>
                <dd>{FIGURE_NAMES.get(type)}</dd>
                <dt>Pole:</dt>
                <dd>{fieldDimensions} pikseli</dd>
                <dt>Wymiary:</dt>
                <dd>
                    {figurePosition.right - figurePosition.left} x{" "}
                    {figurePosition.bottom - figurePosition.top}
                </dd>
                <dt>Współrzędne początku figury:</dt>
                <dd>
                    x: {figurePosition.left}, y: {figurePosition.top}
                </dd>
            </dl>
        </section>
    )
}

export default SelectedArea
