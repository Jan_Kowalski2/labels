import React from "react"
import { useSelector } from "react-redux"
import { AppState } from "redux/store"

import { getStyles } from "helpers";
import styles from "./info.module.css"


const Info = (): JSX.Element => {
    const cx = getStyles(styles)
    const currentImage = useSelector<AppState, LocalImage|null>(state => state.editor.current)

    if (currentImage) {
        const { name, url, width, height } = currentImage
    
        return (
            <dl className={cx("info")}>
                <dt>Nazwa:</dt>
                <dd>{name}</dd>  
                <dt>Wymiary (szerokość x wysokość):</dt>
                <dd>{width} x {height}</dd>   
                <dt>URL:</dt>
                <dd><a href={url}>{url}</a></dd>  
            </dl>
        )
    }

    return (
        <p>-</p>
    )
}

export default Info