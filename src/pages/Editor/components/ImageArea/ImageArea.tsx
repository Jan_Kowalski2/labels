import React, { useState, useEffect, useRef } from "react"
import { useDispatch } from "react-redux"
import { calculateImageFitToCanvas } from "helpers"
import {
    initialResponseStatus,
    loadingResponseStatus,
    successResponseStatus,
    errorResponseStatus
} from "helpers/responseStatus"

import { getStyles } from "helpers"
import styles from "./imagearea.module.css"

import { editImage } from "redux/db/actions"
import { Canvas } from "../index"

const AREA_PADDING = 20

const cx = getStyles(styles)

const initialDimensions: Dimensions = {
    width: 0,
    height: 0
}

const ImageArea = (props: { image: LocalImage }): JSX.Element => {
    const [image, setImage] = useState<LocalImage>(props.image)
    const [imageStatus, setImageStatus] = useState<ResponseStatus>(initialResponseStatus)
    const [imageDimensions, setImageDimensions] = useState<Dimensions>(initialDimensions)
    
    const rootRef = useRef(document.createElement("section"))
    const dispatch = useDispatch()
    
    useEffect(() => {
        const img: HTMLImageElement = document.createElement("img")
        img.src = props.image.url
        
        img.onload = () => {
            const { width, height } = img
            setImageStatus(successResponseStatus)
            const newImage: LocalImage = {...props.image, width, height }
            setImage(newImage)

            if (props.image.width === undefined || props.image.height === undefined) {
                dispatch(editImage(newImage))
                if (rootRef && rootRef.current) {
                    setImageDimensions(calculateImageFitToCanvas({ width: rootRef.current.offsetWidth - 2 * AREA_PADDING, height: rootRef.current.offsetHeight - 2 * AREA_PADDING }, { width, height }))
                }
            }

            else if (props.image.width > 0 && props.image.height > 0) {
                setImageDimensions({ width: props.image.width, height: props.image.height })
            }
        }

        img.onerror = () => setImageStatus(errorResponseStatus)

    }, [ dispatch, props.image ])

    if (imageStatus === initialResponseStatus || imageStatus === loadingResponseStatus || imageDimensions === initialDimensions) {
        return (
            <section className={cx("root", "event", "loading")} ref={rootRef}>
                <p>
                    <span className={cx("loadingDot")}></span>
                    <span className={cx("loadingDot")}></span>
                    <span className={cx("loadingDot")}></span>
                </p>
            </section>
        )
    }

    if (imageStatus === errorResponseStatus) {
        return (
            <section className={cx("root", "event", "error")}>
                <p>
                    <strong>Coś poszło nie tak.</strong>
                    Obraz nie został załadowany.
                </p>
            </section>
        )
    }

    return (
        <section className={cx("root", "loaded")} ref={rootRef}>
            <img 
                src={image.url} 
                alt="Edytowany obraz"
                style={{
                    ...imageDimensions
                }}
            />
            <Canvas dimensions={imageDimensions} />
        </section>
    )
}

export default ImageArea
