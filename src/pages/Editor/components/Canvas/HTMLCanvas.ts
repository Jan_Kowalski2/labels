import { COLOR } from "constants/index"
import { hexToRgb } from "helpers/color"
import { calculateEllipseField } from "helpers/figure"

const STROKE_DEFAULT_WIDTH = 3
const initialCoords: Coords = { x: 0, y: 0 }

export class HTMLCanvas {

    public canvasEl: HTMLCanvasElement
    public ctx: CanvasRenderingContext2D|null
    public rect: DOMRect|ClientRect

    public activeFigureIndex: number = -1
    public figureMode: FigureType = "RECTANGLE"

    public colors: FigureColors = {
        stroke: COLOR.BLACK,
        fill: COLOR.WHITE
    }

    private isStartClicked: boolean = false
    private isDrawing: boolean = false

    private start: Coords = initialCoords
    private end: Coords = initialCoords

    private createdFigures: CanvasFigure[] = []
    private callbacks: { [key in CanvasEvent]: Function } = {
        "ADD_FIGURE": () => {},
        "SELECT_FIGURE": () => {}
    }

    constructor(canvasEl: HTMLCanvasElement, createdFigures: CanvasFigure[], callbacks?: { [ key in CanvasEvent ]: Function }) {
        this.canvasEl = canvasEl
        this.ctx = canvasEl.getContext("2d")
        this.rect = canvasEl.getBoundingClientRect()
        this.setFigures(createdFigures)

        if (callbacks) {
            this.callbacks = callbacks
        }
        this.initListeners()
    }

    public setFigures(createdFigures: CanvasFigure[]): void {
        this.createdFigures = createdFigures
        this.redraw()
    }

    private prepareRectangleCoords(startCoords: Coords, endCoords: Coords): [number, number,number,number] {
        let startX = startCoords.x
        let startY = startCoords.y
        
        if (startCoords.x > endCoords.x) {
            startX = endCoords.x
        }
    
        if (startCoords.y > endCoords.y) {
            startY = endCoords.y
        }
    
        return [
            startX,
            startY,
            Math.abs(startCoords.x - endCoords.x),
            Math.abs(startCoords.y - endCoords.y)
        ]
    }

    private redraw(): void {
        if (this.ctx !== null) {
            this.ctx.clearRect(0, 0, this.canvasEl.width, this.canvasEl.height)
    
            this.createdFigures.forEach((figure: CanvasFigure, index: number) => this.createFigure(figure, this.activeFigureIndex === index))
        }
    }

    private static areCoordsSame(c1: Coords, c2: Coords): boolean {
        if (c1.x === c2.x && c1.y === c2.y) {
            return true
        }
        return false
    }

    private createFigure(args: CanvasFigure, isFigureActive: boolean = false): void {
        const { start, end, type } = args

        if (this.ctx !== null && !HTMLCanvas.areCoordsSame(start, end)) {
            if (type === "RECTANGLE") {
                this.createRectangle(args, isFigureActive)
            }
            else if (type === "ELLIPSE") {
                this.createEllipse(args, isFigureActive)
            }
        }
    }

    private createEllipse(args: CanvasFigure, isFigureActive: boolean): void {
        const { 
            start: startCoords, 
            end: endCoords,
            stroke,
            fill,
            isPreview
        } = args

        if (this.ctx !== null) {
            const opacity = isFigureActive ? 1 : 0.5
            const strokeColor: RGBColor|null = hexToRgb(stroke)
            const fillColor: RGBColor|null = hexToRgb(fill)
            
            this.ctx.beginPath()
            this.ctx.moveTo(startCoords.x, startCoords.y + (endCoords.y - startCoords.y) / 2);
            this.ctx.bezierCurveTo(startCoords.x, startCoords.y, endCoords.x, startCoords.y, endCoords.x, startCoords.y + (endCoords.y - startCoords.y) / 2);
            this.ctx.bezierCurveTo(endCoords.x, endCoords.y, startCoords.x, endCoords.y, startCoords.x, startCoords.y + (endCoords.y - startCoords.y) / 2);

            this.ctx.lineWidth = STROKE_DEFAULT_WIDTH
            this.ctx.strokeStyle = strokeColor ? `rgba(${strokeColor.r}, ${strokeColor.g}, ${strokeColor.b}, ${isPreview ? 0.2 : opacity})` : stroke
            this.ctx.stroke()
    
            if (fill) {
                this.ctx.fillStyle = fillColor ? `rgba(${fillColor.r}, ${fillColor.g}, ${fillColor.b}, ${isPreview ? 0.2 : opacity})` : fill
                this.ctx.fill()
            }

            this.ctx.closePath()
        }
    }

    private createRectangle(args: CanvasFigure, isFigureActive: boolean): void {
        const { 
            start: startCoords, 
            end: endCoords,
            stroke,
            fill,
            isPreview
        } = args

        const figureCoords: [number, number, number, number] = this.prepareRectangleCoords(startCoords, endCoords)
        
        if (this.ctx !== null) {
            const opacity = isFigureActive ? 1 : 0.5
            const strokeColor: RGBColor|null = hexToRgb(stroke)
            const fillColor: RGBColor|null = hexToRgb(fill)

            this.ctx.lineWidth = STROKE_DEFAULT_WIDTH
            this.ctx.strokeStyle = strokeColor ? `rgba(${strokeColor.r}, ${strokeColor.g}, ${strokeColor.b}, ${isPreview ? 0.2 : opacity})` : stroke
            this.ctx.strokeRect(...figureCoords)

            if (fill) {
                this.ctx.fillStyle = fillColor ? `rgba(${fillColor.r}, ${fillColor.g}, ${fillColor.b}, ${isPreview ? 0.2 : opacity})` : fill
                this.ctx.fillRect(...figureCoords)
            }
        }
    }

    private getCoords(event: MouseEvent): Coords {
        return {
            x: event.pageX - Math.floor(this.rect.left), 
            y: event.pageY - Math.floor(this.rect.top) 
        }
    }

    private getClickedFigureIndex(coords: Coords): number {
        let figureIndex = -1

        this.createdFigures.forEach((figure, index) => {
            if (figureIndex === -1) {
                if (this.isInsideRectangle(coords, figure.start, figure.end)) {
                    figureIndex = index
                }
            }
        })
        
        return figureIndex
    }

    private isInsideRectangle(point: Coords, figureStart: Coords, figureEnd: Coords): boolean {
        const figure: Rectangle = HTMLCanvas.getFigurePosition(figureStart, figureEnd)

        const isInsideX = point.x >= figure.left && point.x <= figure.right
        const isInsideY = point.y >= figure.top && point.y <= figure.bottom

        return (isInsideX && isInsideY)
    }

    public static getFigureField(type: FigureType, figureStart: Coords, figureEnd: Coords): number {
        const position: Rectangle = HTMLCanvas.getFigurePosition(figureStart, figureEnd)
        const { top, right, bottom, left } = position

        if (type === "ELLIPSE") {
            return calculateEllipseField(bottom - top, right - left) 
        }

        return (right - left) * (bottom - top)
    }

    public static getFigurePosition(figureStart: Coords, figureEnd: Coords): Rectangle {
        return {
            top: figureStart.y < figureEnd.y ? figureStart.y : figureEnd.y,
            right: figureEnd.x > figureStart.x ? figureEnd.x : figureStart.x,
            bottom: figureEnd.y > figureStart.y ? figureEnd.y : figureStart.y,
            left: figureStart.x < figureEnd.x ? figureStart.x : figureEnd.x
        }
    }

    private initListeners = (): void => {
        this.canvasEl.addEventListener("mousedown", (event: MouseEvent) => {
            const clickedCoords = this.getCoords(event)
            const clickedFigureIndex = this.getClickedFigureIndex(clickedCoords)

            this.activeFigureIndex = clickedFigureIndex

            this.callbacks["SELECT_FIGURE"](clickedFigureIndex === -1 ? null : clickedFigureIndex)

            if (clickedFigureIndex === -1) {
                this.isStartClicked = true
                this.start = clickedCoords
            }
            else {
                this.redraw()
            }

        })

        this.canvasEl.addEventListener("mouseup", (event: MouseEvent) => {
            if (this.isDrawing) {
                
                this.isDrawing = false
                this.isStartClicked = false

                this.end = this.getCoords(event)

                if (!HTMLCanvas.areCoordsSame(this.start, this.end)) {
                    const newFigure: CanvasFigure = {
                        start: this.start, 
                        end: this.end,
                        type: this.figureMode, 
                        ...this.colors
                    }
    
                    this.callbacks["ADD_FIGURE"](newFigure)
    
                    this.createdFigures.push(newFigure)
    
                    this.redraw()
                }

            }
        })

        this.canvasEl.addEventListener("mousemove", (event: MouseEvent) => {
            if (this.isStartClicked) {
                
                if (!this.isDrawing) {
                    this.isDrawing = true
                }

                this.redraw()

                this.createFigure({
                    start: this.start, 
                    end: this.getCoords(event), 
                    type: this.figureMode,
                    isPreview: true,
                    ...this.colors
                })

            }
        })

        this.canvasEl.addEventListener("mouseleave", () => {
            if (this.isDrawing) {
                this.isDrawing = false
                this.isStartClicked = false
                this.redraw()
            }
        })
    }
}

export default HTMLCanvas