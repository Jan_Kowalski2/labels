import React, { useState, useEffect, useRef } from "react"
import { useSelector, useDispatch } from "react-redux"

import HTMLCanvas from "./HTMLCanvas"
import { AppState } from "redux/store"
import { selectFigure, addFigure } from "redux/editor/actions"

interface CanvasProps {
    dimensions: Dimensions
}

const Canvas = (props: CanvasProps) => {
    const { dimensions } = props
    const { width, height } = dimensions
    const canvasRef = useRef()

    const figureType = useSelector<AppState, FigureType>((state: AppState) => state.editor.figureType)
    const image = useSelector<AppState, LocalImage|null>((state: AppState) => state.editor.current)
    
    const dispatch = useDispatch()
    const [canvas, setCanvas] = useState<HTMLCanvas>()
    const [isCanvasInitialized, setCanvasInitializedState] = useState(false)

    const callbacks = {
        "ADD_FIGURE": (newFigure: CanvasFigure) => dispatch(addFigure(newFigure)),
        "SELECT_FIGURE": (index: number) => dispatch(selectFigure(index))
    }
    
    useEffect(() => {
        if (canvas) {
            canvas.figureMode = figureType
        }
    }, [ figureType, canvas ])
    
    useEffect(() => {
        if (canvas && image) {
            const figures = image.labels.map(label => label.figure)
            canvas.setFigures(figures)
        }
    }, [ image, canvas ])

    useEffect(() => {
        if (canvasRef && canvasRef.current && !isCanvasInitialized && image) {
            const figures = image.labels.map(label => label.figure)
            // @ts-ignore
            const initCanvas = new HTMLCanvas(canvasRef.current, figures, callbacks)
            setCanvasInitializedState(true)
            setCanvas(initCanvas)
            initCanvas.figureMode = figureType
        }
    }, [ callbacks, figureType, image, isCanvasInitialized ])

    return (
        // @ts-ignore
        <canvas ref={canvasRef} width={width} height={height}></canvas>
    )
}

export default Canvas