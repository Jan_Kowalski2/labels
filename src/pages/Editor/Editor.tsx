import React, { useState, useEffect } from "react"
import { Redirect, RouteComponentProps } from "react-router-dom"
import { useSelector, useDispatch } from "react-redux"

import { AppState } from "redux/store";
import { clearEditor, enterEditor } from "redux/editor/actions";

import { ImageArea, Toolbox } from "./components"

import { getStyles } from "helpers";
import styles from "./editor.module.css"

interface MatchParams {
    id: string
}

const Editor = (props: RouteComponentProps<MatchParams>): JSX.Element => {
    const { match: { params } } = props
    const cx = getStyles(styles)

    const [ isError, setError ] = useState(false)
    
    const images = useSelector<AppState, LocalImage[]>(state => state.db.images)
    const selectedImage = useSelector<AppState, LocalImage|null>(state => state.editor.current)
    const dispatch = useDispatch()

    useEffect(() => {
        const selectedImageId = params.id
        const currentImage: LocalImage|undefined = images.find(image => image.id === selectedImageId)
    
        if (typeof currentImage === "undefined") {
            setError(true)
        }
        else {
            dispatch(clearEditor())
            dispatch(enterEditor(currentImage))
        }

    }, [ params.id, images, dispatch ])

    if (isError) {
        return <Redirect to="/404" />
    }

    return (
        <section className={cx("root")}>
            <Toolbox />
            {selectedImage && <ImageArea image={selectedImage} />}
        </section>
    )
}

export default Editor