import React, { useState } from "react"
import { useSelector, useDispatch } from "react-redux"
import { Link } from "react-router-dom"

import { getStyles } from "helpers"
import { InputFile } from "ui"

import { AppState } from "redux/store";
import { addImage } from "redux/db/actions"
import styles from "./gallery.module.css"

interface ImageUploadingProps {
    progress: number
}

interface ListProps {
    progress: number
    isUploading: boolean
    images: LocalImage[]
}

const cx = getStyles(styles)

const Image = (props: LocalImage): JSX.Element => {
    const { id, url, name } = props 

    return (
        <div 
            style={{ backgroundImage: `url(${url})` }}
            className={cx("image")} 
            id={id} 
        >
            <p>{name}</p>
        </div>
    )
}

const ImageUploading = (props: ImageUploadingProps): JSX.Element => {
    const { progress } = props

    return (
        <div className={cx("image", "isUploading")}>
            <strong>Ładowanie zdjęcia...</strong>
            <span>{progress}%</span>
        </div>
    )
}


const List = (props: ListProps): JSX.Element => {
    const { isUploading, progress, images } = props

    if (images.length === 0 && progress === 0) {
        return (
            <p className={cx("emptyList")}>
                Galeria jest pusta. 
                <br/>
                Może warto coś dodać?
            </p>
        )
    }

    return (
        <ul className={cx("list")}>
            {isUploading && <li key="upload"><ImageUploading progress={progress} /></li>}
            {images.map((image: LocalImage) => {
                const { id, url, name } = image
                return (
                    <li key={id}><Link to={`/editor/${id}`}><Image id={id} url={url} name={name} labels={[]} categories={[]} /></Link></li>
                )
            })}
        </ul>
    )
}

const Gallery: React.FC = () => {
    const [ isUploading, setUploading ] = useState(false)
    const [ progress, setProgress ] = useState(0)
    const images = useSelector<AppState, LocalImage[]>(state => state.db.images)
    const dispatch = useDispatch()
    const imagesCount = images.length

    const handleUpload = (state: UploadState) => {
        const { progress } = state

        setUploading(progress < 100)
        setProgress(progress)
    }

    const handleSuccessUpload = (file: UploadedFile) => {
        const { fileId, fileName, url } = file

        const newImage: LocalImage = {
            id: fileId,
            name: fileName,
            url,
            labels: [],
            categories: []
        }

        dispatch(addImage(newImage))
    }

    return (
        <section className={cx("root")}>
            <header className={cx("header")}>
                <p className={cx("count")}>Znaleziono {imagesCount === 1 ? `1 zdjęcie` : `${imagesCount} zdjęć`}.</p>
                <InputFile 
                    id="imageUpload"
                    accept="image/x-png,image/gif,image/jpeg" 
                    onChange={handleUpload}
                    onSuccess={handleSuccessUpload}
                    isUploading={isUploading} 
                />
                <p className={cx("tip")}>Kliknij na zdjęcie, aby przejść do edytora.</p>
            </header>
            <List isUploading={isUploading} images={images} progress={progress} />
            <footer className={cx("footer")}>
                <p>&copy; Piotr Kwiatek. Użyto ikon z Font Awesome (<a href="https://fontawesome.com/license">licencja</a>).</p>
            </footer>
        </section>
    )
}

export default Gallery