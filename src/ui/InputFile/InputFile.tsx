import React from "react"
import { getStyles, uploadImage } from "helpers"
import styles from "./inputfile.module.css"

interface InputFileProps {
    isUploading: boolean
    id: string
    accept?: string
    onChange: (state: UploadState) => void
    onSuccess: (file: UploadedFile) => void
}

const InputFile = (props: InputFileProps): JSX.Element => {
    const cx = getStyles(styles)
    const { isUploading, accept = "*", onChange, onSuccess, id } = props

    const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        const inputFileList: FileList | null = event.target.files

        if (inputFileList !== null) {
            const files = Array.from(inputFileList)
         
            uploadImage(files[0], onChange, onSuccess)
        }
    }

    return (
        <div className={cx("root", { disabled: isUploading })}>
            <label htmlFor={id}>+ Dodaj zdjęcie</label>
            <input
                id={id}
                type="file"
                accept={accept}
                onClick={event => isUploading && event.preventDefault()}
                onChange={handleChange}
            />
        </div>
    )
}

export default InputFile
