import React, { ChangeEvent } from "react"

import { getStyles } from "helpers";
import styles from "./input.module.css"

interface InputProps {
    id: string
    type?: string
    label?: string,
    value: string,
    onChange?: (event: ChangeEvent<HTMLInputElement>) => void
}

const Input = React.forwardRef<HTMLInputElement, InputProps>((props, ref) => {
    const { id, type = "text", label, value, onChange } = props
    const cx = getStyles(styles)

    return (
        <div className={cx("root")}>
            {label && <label htmlFor={id}>{label}:</label>}
            <input type={type} ref={ref} id={id} onChange={onChange} value={value} />
        </div>
    )
})

export default Input