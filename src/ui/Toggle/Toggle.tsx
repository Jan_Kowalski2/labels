import React from "react"
import { getStyles } from "helpers";
import styles from "./toggle.module.css"

interface ToggleProps {
    labelOn: string,
    labelOff: string,
    isChecked: boolean,
    id: string,
    onChange: (isChecked: boolean) => void
}

const Toggle = (props: ToggleProps): JSX.Element => {
    const { isChecked, onChange, labelOn, labelOff, id } = props
    const cx = getStyles(styles)

    return (
        <div className={cx("root", { isChecked })}>
            <input type="checkbox" id={id}></input>
            <label htmlFor={id} onClick={() => onChange(!isChecked)}>{isChecked ? labelOn : labelOff}</label>
        </div>
    )
}

export default Toggle