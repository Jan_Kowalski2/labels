import React from "react"
import { useDispatch, useSelector } from "react-redux"
import { Link } from "react-router-dom"

import { AppState } from "redux/store"
import { setChangeFlag, clearEditor } from "redux/editor/actions"
import { LEAVE_WITHOUT_SAVE } from "constants/index"

import { getStyles } from "helpers"
import styles from "./topnav.module.css"

const TopNav: React.FC = () => {
    const cx = getStyles(styles)
    const dispatch = useDispatch()

    const isChanged = useSelector<AppState, boolean>(state => state.editor.isChanged)

    const handleClick = (event: React.MouseEvent<HTMLAnchorElement>) => {
        if (isChanged) {
            const confirm = window.confirm(LEAVE_WITHOUT_SAVE)
            
            if (confirm) {
                dispatch(clearEditor())
                dispatch(setChangeFlag(false))
            }
            else {
                event.preventDefault()
            }
        }
        else if (!isChanged) {
            dispatch(clearEditor())
        }
    }
    return (
        <nav className={cx("root")}>
            <h1>Labels</h1>
            <Link
                to="/"
                className={cx("backToGallery")}
                onClick={event => handleClick(event)}
                title={isChanged ? "Zapisz zmiany przed opuszczeniem edytora." : ""}
            >
                Biblioteka zdjęć
            </Link>
        </nav>
    )
}

export default TopNav
