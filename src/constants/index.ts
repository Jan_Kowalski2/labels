export const COLOR = {
    BLACK: "#000000",
    WHITE: "#ffffff"
} 

export const FIGURE_NAMES = new Map<FigureType, string>()
FIGURE_NAMES.set("RECTANGLE", "prostokąt")
FIGURE_NAMES.set("ELLIPSE", "elipsa")

export const UNDEFINED_CATEGORY: Category = {
    title: "Bez kategorii",
    color: COLOR.WHITE
}

export const LEAVE_WITHOUT_SAVE = "Czy na pewno chcesz opuścić edytor? Nie zapisałeś postępów edycji."