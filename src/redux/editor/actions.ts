import { SET_FIGURE_TYPE, SET_CATEGORY, ADD_FIGURE, SELECT_FIGURE, CLEAR_EDITOR, SET_CHANGE_FLAG, ENTER_EDITOR } from "./types"
import { getRandomColor } from "helpers"

export function setFigureType(newFigureType: FigureType) {
  return {
    type: SET_FIGURE_TYPE,
    payload: newFigureType
  }
}

export function setCategory(figureIndex: number, title: string) {
  return {
    type: SET_CATEGORY,
    payload: { 
      figureIndex, 
      category: { title, color: getRandomColor() } 
    }
  }
}

export function selectFigure(index: number) {
  return {
    type: SELECT_FIGURE,
    payload: index
  }
}

export function addFigure(newFigure: CanvasFigure) {
  return {
    type: ADD_FIGURE,
    payload: newFigure
  }
}

export function setChangeFlag(isChanged: boolean) {
  return {
    type: SET_CHANGE_FLAG,
    payload: isChanged
  }
}

export function enterEditor(newImage: LocalImage) {
  return {
    type: ENTER_EDITOR,
    payload: newImage
  }
}

export function clearEditor() {
  return {
    type: CLEAR_EDITOR
  }
}