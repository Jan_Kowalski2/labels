import { uniqWith, isEqual } from "lodash"
import { SET_FIGURE_TYPE, SET_CATEGORY, SELECT_FIGURE, ADD_FIGURE, SET_CHANGE_FLAG, CLEAR_EDITOR, ENTER_EDITOR, EditorState, EditorStateActions } from "./types"
import { UNDEFINED_CATEGORY } from "constants/index"

const initialState: EditorState = {
    activeFigureIndex: null,
    figureType: "RECTANGLE",
    isChanged: false,
    current: null
}

export function EditorReducer(
    state = initialState,
    action: EditorStateActions
): EditorState {
    switch (action.type) {
        case SET_FIGURE_TYPE:
            return {
                ...state,
                figureType: action.payload
            }
        case ENTER_EDITOR: 
            return {
                ...state,
                current: action.payload
            }
        case CLEAR_EDITOR: 
            return initialState
        case SET_CHANGE_FLAG:
                return {
                    ...state,
                    isChanged: action.payload
                }
        case SET_CATEGORY: {
            const { figureIndex, category } = action.payload
            const { title, color } = category

            if (state.current) {
                const sameCategory: Category|undefined = state.current.categories.find((category: Category) => category.title === title)
                const newFigures = [...state.current.labels]
    
                if (sameCategory) {
                    newFigures[figureIndex] = {
                        figure: {
                            ...state.current.labels[figureIndex].figure,
                            fill: sameCategory.color
                        },
                        category: sameCategory
                    }

                    return {
                        ...state,
                        isChanged: true,
                        current: {
                            ...state.current,
                            labels: newFigures,
                            categories: uniqWith(newFigures.map(label => label.category), isEqual)
                        }
                    }
                }
    
                newFigures[figureIndex] = {
                    figure: {
                        ...state.current.labels[figureIndex].figure,
                        fill: color
                    },
                    category
                }
                const newCategories = [
                    ...state.current.categories,
                    { title, color }
                ]

                return {
                    ...state,
                    isChanged: true,
                    current: {
                        ...state.current,
                        labels: newFigures,
                        categories: uniqWith(newCategories, isEqual)
                    }
                }
            }
            
            return state
        }
        case SELECT_FIGURE: 
            return {
                ...state,
                activeFigureIndex: action.payload
            }
        case ADD_FIGURE:
            const newFigure: CanvasFigure = action.payload 

            if (state.current) {
                return {
                    ...state,
                    isChanged: true,
                    current: {
                        ...state.current,
                        categories: state.current.categories.length > 0 ? [...state.current.categories] : [ UNDEFINED_CATEGORY ],  
                        labels: [
                            ...state.current.labels, 
                            { figure: { ...newFigure, fill: UNDEFINED_CATEGORY.color }, 
                            category: UNDEFINED_CATEGORY } 
                        ] 
                    }
                }
            }
            return state
        default:
            return state
    }
}
