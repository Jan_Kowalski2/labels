export const SET_FIGURE_TYPE = "SET_FIGURE_TYPE"
export const SET_CATEGORY = "SET_CATEGORY"
export const ADD_FIGURE = "ADD_FIGURE"
export const SELECT_FIGURE = "SELECT_FIGURE"
export const SET_CHANGE_FLAG = "SET_CHANGE_FLAG"
export const ENTER_EDITOR = "ENTER_EDITOR"
export const CLEAR_EDITOR = "CLEAR_EDITOR"

export interface EditorState {
    activeFigureIndex: number|null
    figureType: FigureType
    isChanged: boolean
    current: LocalImage|null
}

export interface setFigureType {
    type: typeof SET_FIGURE_TYPE
    payload: FigureType
}

export interface setCategory {
    type: typeof SET_CATEGORY
    payload: { 
        figureIndex: number, 
        category: Category 
    }
}

export interface selectFigure {
    type: typeof SELECT_FIGURE
    payload: number
}

export interface addFigure {
    type: typeof ADD_FIGURE
    payload: CanvasFigure
}

export interface setChangeFlag {
    type: typeof SET_CHANGE_FLAG
    payload: boolean
}

export interface enterEditor {
    type: typeof ENTER_EDITOR
    payload: LocalImage
}

export interface clearEditor {
    type: typeof CLEAR_EDITOR
}

export type EditorStateActions = enterEditor | clearEditor | setChangeFlag | setFigureType | setCategory | selectFigure | addFigure