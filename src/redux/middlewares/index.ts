import localImagesMiddleware from "./localImages"
import preventCloseMiddleware from "./preventClose"

export default [ localImagesMiddleware, preventCloseMiddleware ]