import { MiddlewareAPI, Middleware, Dispatch, AnyAction } from "redux"
import { LEAVE_WITHOUT_SAVE } from "constants/index"
const preventCloseMiddleware: Middleware<Dispatch> = 
    ({ getState }: MiddlewareAPI) => 
    next => 
    (action: AnyAction) => {
        next(action)
        const isChanged: boolean = getState().editor.isChanged
        if (!isChanged) {
            window.onbeforeunload = function() {}
        }
        else if (isChanged) {
            window.onbeforeunload = function() {
                return LEAVE_WITHOUT_SAVE
            }     
        }
}

export default preventCloseMiddleware