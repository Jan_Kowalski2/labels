import { MiddlewareAPI, Middleware, Dispatch, AnyAction } from "redux"
import { ADD_IMAGE, EDIT_IMAGE } from "redux/db/types";
import { handleLocalImages } from "helpers";

const localImagesMiddleware: Middleware<Dispatch> = 
    ({ getState }: MiddlewareAPI) => 
    next => 
    (action: AnyAction) => {
        if (action.type === ADD_IMAGE || action.type === EDIT_IMAGE) {
            next(action)
            const newLocalImages: LocalImage[] = getState().db.images
            return handleLocalImages.set(newLocalImages)
        }
    return next(action)
}

export default localImagesMiddleware