import { SET_RIGHT_PANEL, UiState, UiActionTypes } from "./types"

const initialState: UiState = {
    isRightPanelShown: false
}

export function UiReducer(
    state = initialState,
    action: UiActionTypes
): UiState {
    switch (action.type) {
        case SET_RIGHT_PANEL:
            return {
                isRightPanelShown: action.payload
            }
        default:
            return state
    }
}
