import { SET_RIGHT_PANEL } from "./types"

export function setRightPanel(isShown: boolean) {
  return {
    type: SET_RIGHT_PANEL,
    payload: isShown
  }
}