export const SET_RIGHT_PANEL = "SET_RIGHT_PANEL"

export interface UiState {
    isRightPanelShown: boolean
}

export interface SetRightPanelAction {
    type: typeof SET_RIGHT_PANEL
    payload: boolean
}

export type UiActionTypes = SetRightPanelAction