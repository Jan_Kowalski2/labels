import { createStore, combineReducers, applyMiddleware } from "redux"
import { composeWithDevTools } from "redux-devtools-extension"
import thunkMiddleware from "redux-thunk"

import middlewares from "redux/middlewares"
import { handleLocalImages } from "helpers"

import { UiReducer } from "redux/ui/reducers"

import { DbReducer } from "redux/db/reducers"
import { DbState } from "redux/db/types";

import { EditorReducer } from "redux/editor/reducers"
import { EditorState } from "redux/editor/types";

const rootReducer = combineReducers({
    ui: UiReducer,
    db: DbReducer,
    editor: EditorReducer
})

// Only for DEV and testing purposes
// const temporaryImagesForLocalStorage: LocalImage[] = [
//     {
//         id: "d5sl4068imka8zsr9jfl",
//         name: "caramel.jpg",
//         url: "http://res.cloudinary.com/dcbjiid6l/image/upload/v1568224453/d5sl4068imka8zsr9jfl.jpg",
//         labels: [],
//         categories: []
//     },
//     {
//         id: "gasyoknlmpfdrif38qyu",
//         name: "food.jpg",
//         url: "http://res.cloudinary.com/dcbjiid6l/image/upload/v1568224714/gasyoknlmpfdrif38qyu.jpg",
//         labels: [],
//         categories: []
//     }
// ]
// handleLocalImages.set(temporaryImagesForLocalStorage)

export type AppState = ReturnType<typeof rootReducer>

export default function configureStore() {
    const middlewareEnhancer = applyMiddleware(thunkMiddleware, ...middlewares)
    const persistedState: { db: DbState, editor: EditorState } = {
        db: {
            images: handleLocalImages.get()
        },
        editor: {
            activeFigureIndex: null,
            figureType: "RECTANGLE",
            isChanged: false,
            current: null
        }
    }

    const store = createStore(
        rootReducer,
        persistedState,
        composeWithDevTools(middlewareEnhancer)
    )

    return store
}