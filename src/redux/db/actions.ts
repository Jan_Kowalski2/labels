import { ADD_IMAGE, EDIT_IMAGE } from "./types"

export function addImage(newImage: LocalImage) {
  return {
    type: ADD_IMAGE,
    payload: newImage
  }
}

export function editImage(newImage: LocalImage) {
  return {
    type: EDIT_IMAGE,
    payload: newImage
  }
}