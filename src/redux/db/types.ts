export const ADD_IMAGE = "ADD_IMAGE"
export const ENTER_EDITOR = "ENTER_EDITOR"
export const EDIT_IMAGE = "EDIT_IMAGE"
export const LOCAL_STORAGE_IMAGES_KEY = "LABELS_IMAGES"

export interface DbState {
    images: LocalImage[]
}

export interface AddImage {
    type: typeof ADD_IMAGE
    payload: LocalImage
}

export interface editImage {
    type: typeof EDIT_IMAGE
    payload: LocalImage
}

export type DbStateActions = AddImage | editImage