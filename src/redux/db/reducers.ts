import { cloneDeep } from "lodash"
import { ADD_IMAGE, EDIT_IMAGE, DbState, DbStateActions } from "./types"
import { handleLocalImages } from "helpers"

const LocalImages: LocalImage[] = handleLocalImages.get()

const initialState: DbState = {
    images: LocalImages,
}

export function DbReducer(
    state = initialState,
    action: DbStateActions
): DbState {
    switch (action.type) {
        case ADD_IMAGE:
            return {
                ...state,
                images: [ { ...action.payload }, ...state.images ]
            }
        case EDIT_IMAGE:
            const { id } = action.payload
            const editedImageIndex = state.images.findIndex((el: LocalImage) => el.id === id)
            
            if (editedImageIndex === -1) {
                return {
                    ...state
                }
            }

            const newImages = cloneDeep(state.images)
            newImages[editedImageIndex] = { ...action.payload }

            return {
                ...state,
                images: newImages
            }
        default:
            return state
    }
}
