import React from "react"
import history from "./browserHistory"
import { Router, Redirect, Route, Switch } from "react-router-dom"

import { Gallery, Editor, NotFound } from "pages"
import { TopNav } from "ui"

const App: React.FC = () => {
    return (
      <Router history={history}>
        <TopNav />
        <Switch>
          <Route path="/editor/:id" component={Editor} />
          <Route path="/" component={Gallery} exact />
          <Route path="/" component={NotFound} />
          <Route path="/404" component={NotFound} />
          <Redirect from="*" to="/404" />
        </Switch>
      </Router>
    )
}

export default App
