type Cx = (...styles: any) => string

type LocalImage = {
    url: string
    name: string
    id: string
    width?: number
    height?: number
    labels: Label[]
    categories: Category[]
}

type Label = {
    figure: CanvasFigure
    category: Category
}

type Rectangle = {
    top: number
    right: number
    bottom: number
    left: number
}

type Category = {
    title: string
    color: string
}

type RGBColor = {
    r: number
    g: number
    b: number
}

interface Coords {
    x: number
    y: number
}

type FigureType = "RECTANGLE" | "ELLIPSE"
type CanvasEvent = "SELECT_FIGURE" | "ADD_FIGURE"

interface FigureColors {
    stroke: string
    fill: string
}

interface CanvasFigure extends FigureColors {
    start: Coords
    end: Coords
    type: FigureType
    isPreview?: boolean
}

interface Dimensions {
    width: number
    height: number
}

interface ResponseStatus {
    isLoading: boolean
    isLoaded: boolean
    isError: boolean
}

interface UploadState {
    progress: number
    error: string
}

interface UploadedFile {
    fileId: string
    fileName: string
    url: string
}